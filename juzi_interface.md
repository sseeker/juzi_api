# IT桔子移动端接口

## 整体说明
* 接口地址 http://api.itjuzi.com
* 接口统一向服务器POST，字段格式为 **字段名 --- 是否必填 --- 说明 --- 样例数据**

### 登录注册

AA-1 -- 注册

	接口地址
	 	/user/register
	接口字段
		$user_name  ---  必填  ---  用户昵称 --- singleseeker_for_test
		$user_email  ---  必填  ---  用户Email地址 --- 671826@qq.com 
		$user_pwd  ---  必填  ---  用户密码 --- 671826@qq.com
		$user_scope  ---  必填  ---  用户身份 --- 共三种  1-创业者  2-投资人   3-其他
	返回数据
		register: {
			result_num: null,
			result_msg: "error",
			error_alert: {
				error: [
					"用户名已被占用",
					"该邮箱被注册过"
				]
			}
		}
| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| result_num | 不成功会为 null, 成功后会为插入的ID值  |   |
| result_msg | 成功 ok, 失败 error  |   |
| error_alert | 当用户注册不成功时，会以数组形式返回用户不合法的条目  |   |

	接口地址
	 	/user/send_sms
	接口字段
		$phone  ---  必填  ---  用户手机号 --- 13552768399
	返回数据：
		phone_cms: {
			status: "ok",
			detail: {
				phone_no: "13552768399",
				phone_cms: "828732",
				create_time: 1405167119
			}
		}	
| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| status | 成功时返回ok 失败 error  |   |
| detail | 分别分请求的手机号，得到的验证码，与发送时间  |   |


存储用户的 token 

	接口地址
	 	/user/user_token
	接口字段
		$token  ---  必填  ---  用户手机token
		type	---  必填  --- ios | android
	返回数据：
		result: {
			result_num : 10,
			result_msg : "ok"
		}	

| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| result_num | 10 | 数据库中插入的ID, 为null时表明不可操作 |
| result_msg | 相关的提示 |  文本 |

找回密码时请求发送短信

	接口地址
	 	/user/send_sms_get_pwd 
	接口字段
		$phone  ---  必填  ---  用户手机号 --- 13552768399
	返回数据：
		phone_cms: {
			status: "ok",
			detail: {
				phone_no: "13552768399",
				phone_cms: "828732",
				create_time: 1405167119
			}
		}	
| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| status | 成功时返回ok 失败 error  |   |
| detail | 分别分请求的手机号，得到的验证码，与发送时间  |   |


验正手机号码的短信

	接口地址
	 	/user/verify_sms
	接口字段
		phone  ---  必填  ---  用户手机号 --- 13552768399
		code --- 必填 --- 用户得到的
	返回数据：
		result: {
			result_num : 10,
			result_msg : "ok"
		}	

| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| result_num | 10 | 数据库中插入的ID, 为null时表明不可操作 |
| result_msg | 相关的提示 |  文本 |

保存修改的手机号
		
	接口地址
	 	/user/change_mobile
	接口字段
		phone  ---  必填  ---  用户手机号 --- 13552768399
		code --- 必填 --- 用户得到的
	返回数据：
		result: {
			result_num : 10,
			result_msg : "ok"
		}	

| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| result_num | 10 | 数据库中插入的ID, 为null时表明不可操作 |
| result_msg | 相关的提示 |  文本 |

修改的密码
		
	接口地址
	 	/user/change_pwd
	接口字段
		old  ---  必填  ---  用户旧密码 --- 123123 为 __itjuzi_app_hook__  时为万能密码，配合手机更改密码使用
		new --- 必填 --- 用户新密码 --- 123213123213
	返回数据：
		result: {
			result_num : 10,
			result_msg : "ok"
		}	

| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| result_num | 10 | 数据库中插入的ID, 为null时表明不可操作 |
| result_msg | 相关的提示 |  文本 |

修改的密码
		
	接口地址
	 	/user/find_pwd
	接口字段
		phone  ---  必填  --- 用户的手机号
		new --- 必填 --- 用户新密码 --- 123213123213
	返回数据：
		result: {
			result_num : 10,
			result_msg : "ok"
		}	

| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| result_num | 10 | 数据库中插入的ID, 为null时表明不可操作 |
| result_msg | 相关的提示 |  文本 |


用户更新app
		
	接口地址
	 	/user/update_new_version
	接口字段
		version  ---  必填  --- 用户的当前的版本号
	返回数据：
		result: {
			id = 1,
			flag = 1;
           		new_version = 1.0.1;
            		mandatory_version = 1.0.0;
           		info = "新版本出来了，赶快去更新把！";
           		url = "www.baidu.com";
           
		}	


AA-2 -- 登陆

	接口地址
	 	/user/login
	接口字段
		user  ---  必填  ---  用户Email地址 --- 671826@qq.com
		password --- 必填  ---  用户密码 --- 123456
	返回数据
		{
			session_data: {
				cookie_data: "a:4:{s:10:"session_id";s:32:"7eea250176edc43d97592729ec5ff0ea";s:10:"ip_address";s:9:"127.0.0.1";s:10:"user_agent";s:35:"Guzzle/3.9.1 curl/7.30.0 PHP/5.4.24";s:13:"last_activity";i:1401846141;}05e10011003ca2fc9c178c0455746c81"
			},
			basic: {
				site_name: "IT桔子"
			},
			user: {
				session_id: "7eea250176edc43d97592729ec5ff0ea",
				ip_address: "127.0.0.1",
				user_agent: "Guzzle/3.9.1 curl/7.30.0 PHP/5.4.24",
				last_activity: 1401846141,
				user_data: "",
				identity: "671826@qq.com",
				username: "singleseeker",
				email: "671826@qq.com",
				user_id: "3",
				old_last_login: "1401846141",
				real_name: "ss",
				mobile: " ",
				logo: "73761612673.png",
				role: "1",
				company_claim_title: "",
				company_claim_name: "",
				prov: "北京"，
				city: "北京"
				
			}
		}
	说明		
		1. session_data 做为用户登陆凭证，后续所有登陆用户要向服务器POST发送 session 字段，内容为 session_data 下 cookie_data 中的字符串，保证用户登陆态，即发送 "a:4:{s:10:"session_id";s:32:"7eea250176edc43d97592729ec5ff0ea";s:10:"ip_address";s:9:"127.0.0.1";s:10:"user_agent";s:35:"Guzzle/3.9.1 curl/7.30.0 PHP/5.4.24";s:13:"last_activity";i:1401846141;}05e10011003ca2fc9c178c0455746c81"。	推荐首次登陆后，存入本地。
		2. 验证用户是否登陆，查看 user->identity 是否有值，如果用户登陆，则有当前用户的 email，否则用户没有登陆。
		3. 所有请求都会返回 session_data, basic, user, 后续的接口中为节省版面，不贴出此部分代码。



AA-more -- 退出登陆

	接口地址
	 	/user/logout
	接口字段
		
	返回数据
		{
			session_data: {
				cookie_data: "a:4:{s:10:"session_id";s:32:"7eea250176edc43d97592729ec5ff0ea";s:10:"ip_address";s:9:"127.0.0.1";s:10:"user_agent";s:35:"Guzzle/3.9.1 curl/7.30.0 PHP/5.4.24";s:13:"last_activity";i:1401846141;}05e10011003ca2fc9c178c0455746c81"
			},
			basic: {
				site_name: "IT桔子"
			},
			user: {
				session_id: "7eea250176edc43d97592729ec5ff0ea",
				ip_address: "127.0.0.1",
				user_agent: "Guzzle/3.9.1 curl/7.30.0 PHP/5.4.24",
				last_activity: "1401846141",
				user_data: " "
			}
		}


得到用户隐私设置

	接口地址	
		/user/get_user_privacy
	接口字段
		
	返回数据
		{
			'show_weibo'  : 1,
			'show_qq'     : 0,
			'show_mobile' : 1,
			'show_mail'   : 1
		}
	
	说明：1为显示，0为不显示


		

		
AA-3 -- 手机验证

暂未提供

AA-4 -- 选择你喜欢的领域

	接口地址：
		/user/all_scope
	接口字段：
		
		
	返回数据：
		scope: [
				{
				id: 1,
				img: "650b1406baf39292198e3ba5e1e71b06.jpg",
				title: "2014中国互联网发展广告"
				}
		]
		
| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| id | 领域ID  |   |
| img | 领域图  |   |
| title | 领域标题  |   |

AA-5 -- 选择你喜欢的圈子


	接口地址：
		/user/all_circle
	接口字段：
		
		
	返回数据：
		circle: [
				{
				id: 1,
				img: "650b1406baf39292198e3ba5e1e71b06.jpg",
				title: "2014中国互联网发展广告"
				}
			]
		
| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| id | 圈子ID  |   |
| img | 圈子图  |   |
| title | 圈子标题  |   |

AA-more 点击选择自己喜爱的圈子或领域

	接口地址：
		/user/user_select_follow
	接口字段：
		handle_id  ---  必填  ---  圈子或领域的ID --- 1
		handle_type  ---  必填  ---  圈子或领域的ID --- 领域为 scope | 圈子为 circle
	返回数据：
		user_select_follow: {
			result_num : 10,
			result_msg : "ok"
		}	

| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| result_num | 10 | 数据库中插入的ID, 为null时表明不可操作 |
| result_msg | 相关的提示 |  文本 |	

AA-more 点击选择自己喜爱的圈子或领域

	接口地址：
		/user/user_unselect_follow
	接口字段：
		handle_id  ---  必填  ---  圈子或领域的ID --- 1
		handle_type  ---  必填  ---  圈子或领域的ID --- 领域为 scope | 圈子为 circle
	返回数据：
		result: {
			result_num : 10,
			result_msg : "ok"
		}	

| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| result_num | 10 | 数据库中插入的ID, 为null时表明不可操作 |
| result_msg | 相关的提示 |  文本 |	



### 公司库

B-1 -- 公司库
	
	接口地址
	 	/company/all
	接口字段
		session  ---  必填  ---  验证登陆串 --- "a:4:{s:10:"session_id";s:32:"7eea250176edc43d97592729ec5ff0ea";s:10:"ip_address";s:9:"127.0.0.1";s:10:"user_agent";s:35:"Guzzle/3.9.1 curl/7.30.0 PHP/5.4.24";s:13:"last_activity";i:1401846141;}05e10011003ca2fc9c178c0455746c81"
		page  ---  选填  ---  第几页 --- 1
	返回数据
		{
			company_list: [
				{
					{
						com_id: "6",
						com_des: "E达招车是一个手机智能打车应用，支持一键呼叫、即时为用户安排合适的出租车等。",
						com_logo: "86199434ffaacca095eef629f6378375.png",
						com_prov: "北京",
						com_city: "朝阳区",
						com_scope_id: "19",
						com_name: "E达招车 ",
						com_stage_id: "2",
						com_registered_name: "北京E达招车有限公司",
						stage: {
							id: "2",
							name: "成长发展期"
						},
						scope: {
							id: "19",
							name: "汽车交通"
						},
						comment: {
							num: "0"
						},
						follow: {
							num: "1"
						},
						is_claim: true,
						is_fund: false,
						is_new: true,
						is_hot: false
						
					}																	
				}
			],
		}		

| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| com_id | 公司ID  |   |
| com_des | 公司描述  |   |
| com_registered_name | 公司名称字段 |
| com_logo |公司LOGO地址 | 前缀为 http://itjuzi.com/images/   |
| com_prov | 省  |   |
| com_city | 市  |   |
| com_scope_id | 领域ID  |  与 scope->id 值一致 |
| scope| 领域对象 |   | 
| comment | 评论数  |   |
| follow | 关注数  |   |
| is_claim: true | 只有四种状态， 是否为 ‘认’ |
| is_fund: false | 只有四种状态， 是否为 ‘融’ |
| is_new: true | 只有四种状态， 是否为 ‘新’ |
| is_hot: false |  只有四种状态， 是否为 ‘荐’ |

	接口地址
	 	/company/search
	接口字段
		$key --- 必填  --- 用户输入的关键字 -- 美丽说
		$scope --- 必填  --- 领域 ----1
		$stage --- 必填  --- 阶段 ----1
		$status --- 必填  --- 状态 ----1
		$fund_status --- 必填  --- 融资 ----1
		$prov --- 必填  --- 地点 ----1
		$born_year --- 必填  --- 时间 ----1
		$order --- 必填  --- 排序 ---- 1
	返回数据
		{
			company_list: [
				{
					{
						com_id: "6",
						com_des: "E达招车是一个手机智能打车应用，支持一键呼叫、即时为用户安排合适的出租车等。",
						com_logo: "86199434ffaacca095eef629f6378375.png",
						com_prov: "北京",
						com_city: "朝阳区",
						com_scope_id: "19",
						com_name: " E达招车",
						com_registered_name: " 北京E达招车有限公司",
						stage: {
							id: "2",
							name: "成长发展期"
						},
						scope: {
							id: "19",
							name: "汽车交通"
						},
						comment: {
							num: "0"
						},
						follow: {
							num: "1"
						},
						is_claim: true,
						is_fund: false,
						is_new: true,
						is_hot: false
									
					}	
							
				}
			],
		}		

| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| com_id | 公司ID  |   |
| com_des | 公司描述  |   |
| com_registered_name | 公司名称字段 |
| com_logo |公司LOGO地址 | 前缀为 http://itjuzi.com/images/   |
| com_prov | 省  |   |
| com_city | 市  |   |
| com_scope_id | 领域ID  |  与 scope->id 值一致 |
| scope| 领域对象 |   | 
| comment | 评论数  |   |
| follow | 关注数  |   |
| is_claim: true | 只有四种状态， 是否为 ‘认’ |
| is_fund: false | 只有四种状态， 是否为 ‘融’ |
| is_new: true | 只有四种状态， 是否为 ‘新’ |
| is_hot: false |  只有四种状态， 是否为 ‘荐’ |


B-3 公司库
	
	接口地址
	 	/company/get_company_detail_by_id
	接口字段
		id  ---  必填  ---  公司ID --- 1
	返回数据
		comapny: {
			com_id: "1",
			com_name: "美丽说",
			com_registered_name: "北京美丽时空网络科技有限公司",
			com_logo: "e05c9133389b94c52413454a99abfcc1.png",
			com_logo_180: "",
			com_des: "美丽说是一个女性时尚社区和社会化电商平台，致力于为女性用户提供最美丽的时尚购物体验，用互联网技术为她们解决“怎么穿，哪里买”的问题。",
			com_from_url: "",
			com_url: "http://www.meilishuo.com",
			com_born_year: "2009",
			com_born_month: "11",
			com_prov: "北京",
			com_city: "海淀区",
			com_status_id: "1",
			com_stage_id: "2",
			com_scope_id: "2",
			com_fund_status_id: "8",
			com_scope_extend_id: null,
			com_re_edit_user_id: "0",
			com_re_edit_com_id: "0",
			com_show: "0",
			com_location: "in",
			com_recommend: "0",
			share_url: 'mobile/share/company/1',
			stage: {
				id: "2",
				name: "成长发展期"
			},
			
			scope: {
				id: "2",
				name: "电子商务"
			},
			status: {
				id: "1",
				name: "运营中"
			},
			tag: [
					{
						com_tag_id: "60",
						com_tag_name: "时尚",
						com_tag_num: "101"
					},
					{
						com_tag_id: "61",
						com_tag_name: "女性应用",
						com_tag_num: "93"
					},
					{
						com_tag_id: "126",
						com_tag_name: "社会化电子商务",
						com_tag_num: "152"
					},
					{
						com_tag_id: "127",
						com_tag_name: "导购",
						com_tag_num: "442"
					}
				],
			member: [
				{
					per_id: "1",
					per_name: "徐易容",
					per_logo: "43c5016e095d549504cf7910dae58692.png",
					per_des: "1975年出生，1997年毕业于北京大学计算机系，2000年获得美国斯坦福大学计算机硕士学位，同年加入世界著名的IBM阿莫顿研究中心，在硅谷从事数据挖掘研究，2003年回国加入IBM中国软件开发中心（CDL）。徐易容在互联网、数据挖掘、数据管理领域创造了多项新技术，共同发明了5项专利。2005年年底开始创业创办了抓虾RSS阅读器。在2009年11月创办了美丽说。",
					des: "CEO"
				}
			],			
			fund: [
				{
					invse_id: "113",
					invse_year: "2012",
					invse_month: "10",
					com_id: "1",
					com_name: "美丽说",
					invse_round_name: "D轮",
					invse_detail_money: "0",
					invse_similar_money_name: "数千万",
					invse_currency_name: "美元",
					com_scope_name: "电子商务",
					investment: "腾讯产业共赢基金,45"
				},
				{
					invse_id: "5",
					invse_year: "2011",
					invse_month: "10",
					com_id: "1",
					com_name: "美丽说",
					invse_round_name: "C轮",
					invse_detail_money: "2000",
					invse_similar_money_name: "数千万",
					invse_currency_name: "美元",
					com_scope_name: "电子商务",
					investment: "红杉资本,1|蓝驰创投,8|纪源资本GGV,20|清科创投,21"
				},
				{
					invse_id: "4",
					invse_year: "2011",
					invse_month: "5",
					com_id: "1",
					com_name: "美丽说",
					invse_round_name: "B轮",
					invse_detail_money: "800",
					invse_similar_money_name: "数百万",
					invse_currency_name: "美元",
					com_scope_name: "电子商务",
					investment: "红杉资本,1"
				},
				{
					invse_id: "1",
					invse_year: "2010",
					invse_month: "12",
					com_id: "1",
					com_name: "美丽说",
					invse_round_name: "A轮",
					invse_detail_money: "200",
					invse_similar_money_name: "数百万",
					invse_currency_name: "美元",
					com_scope_name: "电子商务",
					investment: "蓝驰创投,8"
				}
			],
			fund_status: {
				com_fund_status_id: "8",
				com_fund_status_name: "已完成D轮融资",
				com_fund_status_num: "29"
			},
			mile_stone: [
				{
					com_mil_id: "1",
					com_mil_detail: "美丽说正式上线",
					com_mil_year: "2009",
					com_mil_month: "11"
				},
				{
					com_mil_id: "3",
					com_mil_detail: "美丽说获得蓝驰创投第一轮200万美元投资",
					com_mil_year: "2010",
					com_mil_month: "12"
				},
				{
					com_mil_id: "12",
					com_mil_detail: "美丽说发布iOS及Android客户端，正式布局移动互联网业务",
					com_mil_year: "2011",
					com_mil_month: "4"
				},
				{
					com_mil_id: "6",
					com_mil_detail: "美丽说获得红杉资本第二轮800万美元投资",
					com_mil_year: "2011",
					com_mil_month: "5"
				},
				{
					com_mil_id: "9",
					com_mil_detail: "美丽说获得第三轮2000万美元投资，纪源资本领投，红杉资本、蓝驰创投、清科创投跟投",
					com_mil_year: "2011",
					com_mil_month: "10"
				},
				{
					com_mil_id: "15",
					com_mil_detail: "美丽说对外宣布移动客户端安装量已超过1000万",
					com_mil_year: "2012",
					com_mil_month: "8"
				},
				{
					com_mil_id: "14",
					com_mil_detail: "发布家居生活、欧美时尚派对、森之光、粉陶罐子、异域风情-精选民族风等多款iOS App",
					com_mil_year: "2012",
					com_mil_month: "9"
				},
				{
					com_mil_id: "13",
					com_mil_detail: "发布“美丽计划”App客户端",
					com_mil_year: "2012",
					com_mil_month: "10"
				}
			]
			
		}
| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| com_id | 公司ID  |   |
| com_des | 公司描述  |   |
| com_logo |公司LOGO地址 | 前缀为 http://itjuzi.com/images/   |
| com_prov | 省  |   |
| com_city | 市  |   |
| com_scope_id | 领域ID  |  |
| scope| 领域对象 |   | 
| status| 领域对象 |   | 
| tag| 领域对象 |  com_tag_num 为当前tag有多少家公司，暂不需要 | 
| member| 领域对象 |  des为职位  | 
| 融资相关 |  | 说明待补充  | 
| 里程相关 |  | 说明待补充  | 
| share_url |  | 分享的url  | 


B-5 专辑与评论功能

	接口地址：
		/company/album
	接口字段：
		page  ---  选填  ---  第几页 --- 1
	返回数据：
		album: [
			{
				album_id: "43",
				album_name: "基于iBeacon技术的创业公司",
				album_des: "iBeacon是苹果公司正在倡导和推广的室内定位技术，看看国内有哪些技术服务商",
				album_company_count: "4",
				album_time: "2014-04-26 10:17:42",
				share_url: 'mobile/share/album/1'
				album_detail: [
					{
						com_id: "9546",
						com_name: "DropBeacon水滴系统",
						com_logo: "f4ea2ed7050d944cc9c6aa394540c19c.jpg",
						is_claim: false,
						is_fund: false,
                    				is_new: true,
                   				is_hot: false,
                   				com_scope_id: "2",
                   				com_create:  null,
                   				scope:{
                   					id: "2",
                   					name: "电子商务"
                   				}
       					}
				]							
			}
		]
字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| album_id | 专辑描述 |  |
| album_name | 专辑名称  |   |
| album_des | 专辑描述  |   |
| album_company_count | 专辑包含公司总数  |   |
| album_time | 专辑创建时间  |   |
| share_url | 分享的url  |   |


获取专辑包含公司接口
		
	接口地址：
		/company/album_detail
	接口字段：
		id  ---  必填  ---  专辑ID --- 1
	返回数据：
		album_detail: [
			{
				com_id: "6593",
				com_name: "厦门好慷家政HomeKing",
				com_logo: "32fb13c23962cad1d1058871e3310e5e.jpg",
				com_created: null,
				com_scope_id: "2",
				scope:{
					id:"11"
					name:"消费生活"
				}
				is_claim: false,
				is_fund: true,
				is_new: false,
				is_hot: false
			}		
		]
字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| com_id | 公司ID |  |
| com_name | 公司名称  |   |
| com_logo | 公司LOGO地址  |   |

评论公司接口		
		
	接口地址：
		/company/add_comment	
	接口字段：
		comment_info  ---  必填  ---  评论内容 --- 我中一个评论ID
		com_id ---  必填  ---  公司ID --- 1
	返回数据：
		comapny_add: {
			result_num : 10,
			result_msg : "ok"
		}	
字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| result_num | 10 | 数据库中插入的ID, 为null时表明不可操作 |
| result_msg | 相关的提示 |  文本 |

评论专辑

	接口地址：
		/company/post_album_comment
	接口字段：
		info  ---  必填  ---  评论内容 --- 我中一个评论ID
		id ---  必填  ---  专辑ID --- 1
	返回数据：
		result: {
			result_num : 10,
			result_msg : "ok"
		}	
字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| result_num | 10 | 数据库中插入的ID, 为null时表明不可操作 |
| result_msg | 相关的提示 |  文本 |

得到专辑评论列表

	接口地址：
		/company/get_album_comment
	接口字段：
		pge  ---  必填  ---  页数
		id ---  必填  ---  专辑ID --- 1
	返回数据：
		result: {
			{
				id: 1				
				logo: "650b1406baf39292198e3ba5e1e71b06.jpg",
				username: "wenfeixiang",
				user_id: 10,
				time: "1398766500",
				comment_info: "这个主题写的好啊。我真的好喜欢"
			}
		}	

	


B-7 添加公司

	接口地址：
		/company/add_com_by_user
	接口字段：
		com_name  ---  必填  ---  公司各称 --- it桔子
		com_url ---  必填  ---  公司网址 --- baidu.com
		com_des ---  必填  ---  公司描述 --- 这是家公司
	返回数据：
		comapny_add: {
			result_num : 10,
			result_msg : "ok"
		}	

| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| result_num | 10 | 数据库中插入的ID, 为null时表明不可操作 |
| result_msg | 相关的提示 |  文本 |
	

B-8 认领公司

	接口地址：
		/company/claim_com
	接口字段：
		com_id  ---  必填  ---  公司ID --- it桔子
		claim_user_name ---  必填  ---  认领人姓名 --- 麻花疼
		claim_user_title ---  必填  ---  认领人职位 --- ceo
		claim_user_mobile ---  必填  ---  认领人电话 --- 13123213
		claim_user_email ---  必填  ---  认领人邮件 --- xxx@qq.com
		claim_user_card ---  选填  ---  图片 --- 
	返回数据：
		comapny_claim: {
			result_num : 10,
			result_msg : "ok"
		}	

| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| result_num | 10 | 数据库中插入的ID, 为null时表明不可操作 |
| result_msg | 相关的提示 |  文本 |	
		
		

B-more 关注公司

	接口地址：
		/company/follow_com
	接口字段：
		com_id  ---  必填  ---  公司id --- 1
	返回数据：
		comapny_follow: {
			result_num : 10,
			result_msg : "ok"
		}	

| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| result_num | 10 | 数据库中插入的ID, 为null时表明不可操作 |
| result_msg | 相关的提示 |  文本 |	

B-more 纠错

	接口地址：
		/company/user_correct_company
	接口字段：
		com_id  ---  必填  ---  公司id --- 1
		info  ---  必填  ---  纠错内容 --- 其实公司的地址写错了
	返回数据：
		comapny_follow: {
			result_num : 10,
			result_msg : "ok"
		}	

| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| result_num | 10 | 数据库中插入的ID, 为null时表明不可操作 |
| result_msg | 相关的提示 |  文本 |	

	
	

B-9 新闻

	接口地址：
		/company/get_company_news_by_id
	接口字段：
		id  ---  必填  ---  公司ID --- 1
	返回数据：
		comapny_news: [
			{
				com_new_id: "9040",
				com_new_name: "美丽说：遭遇封杀后怎么活下去？",
				com_new_url: "http://tech.163.com/14/0224/09/9LRBG81D000915BF.html",
				com_new_year: "2014",
				com_new_month: "2",
				com_new_day: "24",
				com_new_type_id: "4",
				com_new_show: "0",
				rel_com_with_new_id: "9040",
				com_id: "1",
				com_new_type_name: "分析评论",
				com_new_type_num: "1958"
			}
		]
| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| com_new_id | 新闻ID  |   |
| com_new_name | 新闻标题  |   |
| com_new_url | 新闻来源URL  |   |
| com_new_year | 新闻发布 - 年  |   |
| com_new_month | 新闻发布 - 月  |   |
| com_new_day| 新闻发布 - 日  |   |
| com_new_type_name | 新闻类型  |   |

B-10 相关公司

	接口地址：
		/company/get_others_detail_by_id
	接口字段：
		id  ---  必填  ---  公司ID --- 1
		scope_id  ---  必填  ---  要查找相似公司的领域ID --- 1
	返回数据：
		comapny_others: [
			{
				com_id: "8571",
				com_name: "微蜜/蜜客MeCare",
				com_registered_name: "上海蜜客信息技术有限公司",
				com_logo: "ae2c74ae71cd4c9b23fb4aa46c18aaf6.jpg",
				com_logo_180: "",
				com_des: "微蜜是一个为微信平台企业公共账号提供服务的工具，隶属于上海蜜客信息技术有限公司。",
				com_from_url: "",
				com_url: "http://wm.mecare.me/",
				com_born_year: "2013",
				com_born_month: "10",
				com_prov: "上海",
				com_city: "浦东新区",
				com_status_id: "1",
				com_stage_id: "1",
				com_scope_id: "1",
				com_fund_status_id: "2",
				com_scope_extend_id: null,
				com_re_edit_user_id: "0",
				com_re_edit_com_id: "0",
				com_show: "0",
				com_location: "in",
				com_recommend: "0",
				is_claim: false,
				is_fund: false,
          			is_new: true,
          			is_hot: false,
               			com_create: null,
             	 		scope: {
             	 			id: "2",
             	 			name: "电子商务"
             	 		}
			}
		]
| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| com_id | 公司ID  |   |
| com_des | 公司描述  |   |
| com_logo |公司LOGO地址 | 前缀为 http://itjuzi.com/images/   |
| com_prov | 省  |   |
| com_city | 市  |   |
| com_scope_id | 领域ID  |  |

B-11 评论

	接口地址：
		/company/get_company_comment_by_id
	接口字段：
		id  ---  必填  ---  公司ID --- 100（此ID下有评论，有些ID下无评论）
	返回数据：
		comapny_comment: [
			{
				commont_id: "49",
				com_id: "100",
				user_id: "7",
				commont_detail: "据说这家公司的年收入过2亿元了。。隐藏的好公司",
				commont_time: "1371311824",
				logo: "7ea117210711a8324c5a8409ef85a714.jpg",
				username: "IT桔子管理员账号",
				id: "7"
			}
		]
| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| commont_id | 评论ID  |   |
| user_id | 评论用户ID  |   |
| logo |  评论用户头像  |   |
| username | 评论用户昵称  |   |
| commont_time | 评论时间  |   |
| commont_detail | 评论内容  |   |


### 个人主页


Z-more 处理消息

	接口地址：
		/user/handle_connect
	接口字段：
		connect_id  ---  必填  ---  消息的id
		handle -- 必填  ---- 通过或是忽略    pass | ignore
	返回数据：
		result: {
			result_num : 10,
			result_msg : "ok"
		}	

| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| result_num | 10 | 数据库中插入的ID, 为null时表明不可操作 |
| result_msg | 相关的提示 |  文本 |	


Z-more 消息列表

	接口地址：
		/user/user_get_msg
	接口字段：
		$message_id --- 可选 --- 默认返回所有，填写此项后只返回当前的消息
	返回数据：
		msg_list: [
			{
				message_id:5
				type:answer_talk,
				user_name: '谢天',
				user_id: '1',
				user_logo: 'xxxx.png',
				info: '你说的太对了',
				time: 1231231,
				handle_id: '44153',
				source_id: '3',
				detail : {
					publish_user_id: "1",
					publish_user_nick_name: "颜刘氏",
					talk_info: "词是诗歌的一种，入乐的叫歌，不入乐的叫诗。入乐的歌在感情抒发、形象塑造上和诗没有任何区别，但在结构上、节奏上要受音乐的制约，在韵律上要照顾演唱的",
					talk_comment_num: "20",
					talk_hot: "30",
					img: [],
					publish_time: "1398766500",
					publish_id: "1",
					share_url: 'mobile/share/talk/1',
					is_plus : 1,
					plus_num : 1,
					is_min : 1,
					min_num :1,
					talk_img:"12847173.png",
					nick_id:"187",
					publish_user_logo:"62365251.jpg"
					
				}				
			}
		]

提供已读的信息
		
	接口地址：
		/user/read_msg
	接口字段：
		$message_id --- 必填 --- 默认返回所有，填写此项后只返回当前的消息
	返回数据：
		result: {
			result_num : 10,
			result_msg : "ok"
		}	

| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| result_num | 10 | 数据库中插入的ID, 为null时表明不可操作 |
| result_msg | 相关的提示 |  文本 |				

Z-more 联系他

	接口地址：
		/user/connect_him
	接口字段：
		connect_id  ---  必填  ---  想联系人的id --- 1
	返回数据：
		result: {
			result_num : 10,
			result_msg : "ok"
		}	

| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| result_num | 10 | 数据库中插入的ID, 为null时表明不可操作 |
| result_msg | 相关的提示 |  文本 |	

Z-more 举报

	接口地址：
		/user/inform
	接口字段：
		inform_word  ---  必填  ---  举报的文字 --- 垃圾
		id   ---  必填  --- 举报内容的ID
		type  ---  必填  ---  话题 topic |  桔说 talk
	返回数据：
		result: {
			'status' => 'error',
			'inform_id' => $inform_id,
			'info' => '重复举报'
		}	

| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| inform_id | 10 | 数据库中的ID |
| info | 相关的提示 |  文本 |	



Z-2 更新个人信息

	接口地址：
		/user/user_info
	接口字段：
		user_id  ---  选填  ---  用户ID（希望得到他人的数据时） --- 1
	返回数据：
		user_info: {
			logo: "650b1406baf39292198e3ba5e1e71b06.jpg",
			username: "wenfeixiang",
			real_name: "文飞翔",
			prov: "北京",
			city: "海淀区",
			des: "IT桔子创始人，产品经理，Blogger",
			skill: [
				{
					id: 1,
					name: "程序员"
				},
				{
					id: 2,
					name: "设计师"
				}
			],
			goal: [
				{
					id: 1,
					name: "找投资"
				},
				{
					id: 2,
					name: "找合伙人"
				}
			],
			show_mobile: 0,
			mobile: "1134214",
			show_email: 1,
			email: "wenfeixiang@gmail.com",
			show_qq: 1,
			qq: "781793279",
			show_weibo: 0,
			weibo: "http://www.weibo.com/wenfeixiang",
			role: 1
		}	
| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| logo | 头像  |   |
| username | 昵称  |   |
| real_name | 名字  |   |
| prov | 地点省  |   |
| city | 地点市  |   |
| des | 个人简介  |   |
| skill | 技能数组  |   |
| goal | 目的数组  | 注册目的  |
| show_mobile | 是否显示手机号  | 0-不显示  1-显示  |
| mobile | 手机号  |  |
| show_email | 是否显示邮箱 |  |
| email | 邮箱 |  |
| show_qq | 是否显示qq |  |
| qq | qq |  |
| show_weibo | 是否显示微博 |  |
| weibo | 微博 |  |

z-2 more

隐私设置

	接口地址：
		/user/privacy_config
	接口参数:
		type  ---  必填  ---  要进行设置的参参数 --- mobile , email,  qq,  weibo
		status  ---  必填  ---  当前参数和的状态 --- on | off
	返回数据：
		config: {
			result_num : 10,
			result_msg : "ok"
		}	

| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| result_num | 10 | 操作数据的ID, 为null时表明不可操作 |
| result_msg | 相关的提示 |  文本 |	

修改用户基本信息

	接口地址：
		/user/edit_user_info
	接口参数:
		type  ---  必填  ---  要进行设置的参参数 --- 邮箱|email, 地址|addr,  昵称|nick_name, 简介|des，用户评分|score, 检查更新|check_update, 微博|weibo qq|qq号，name|用户名字
		value  ---  必填  ---  要设置的置 --- 如果是文本直接传，如果为boolen,则用  on | off 两个值, 地址用 省份,城市
	返回数据：
		config: {
			result_num : 10,
			result_msg : "ok"
		}	

| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| result_num | 10 | 操作数据的ID, 为null时表明不可操作 |
| result_msg | 相关的提示 |  文本 |	

更改用户头像


	接口地址：
		/user/change_logo
	接口参数:
		logo  ---  必填  ---  用户新上传的头像 --- 
	返回数据：
		config: {
			result_num : 10,
			result_msg : "ok"
		}	

| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| result_num | 10 | 操作数据的ID, 为null时表明不可操作 |
| result_msg | 相关的提示 |  文本 |	


设置用户地址 

	接口地址：
		/user/edit_user_addr
	接口参数:
		city  ---  必填  ---  城市 --- 上海
		prov  ---  必填  ---  省份 --- 上海
	返回数据：
		result: {
			result_num : 10,
			result_msg : "ok"
		}	
	说明 这个可以是个联动的菜单。关于数据可以调用 /address 下的相关接口	

| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| result_num | 10 | 操作数据的ID, 为null时表明不可操作 |
| result_msg | 相关的提示 |  文本 |	
	

建议反馈
	
	接口地址：
		/user/user_feed_back
	接口参数:
		info  ---  必填  ---  用户的建议 --- 文本
		user  ---  必填  ---  用户联系方式 --- 文本
	返回数据：
		config: {
			result_num : 10,
			result_msg : "ok"
		}	

| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| result_num | 10 | 操作数据的ID, 为null时表明不可操作 |
| result_msg | 相关的提示 |  文本 |	

Z-more 收藏

	接口地址：
		/user/user_follow
	接口参数:
		handle ---- 必填   ---- 收藏 还是取消 ---- on|off
		type  ---  必填  ---  类型 --- album - 专辑,talk - 桔说,topic - 话题,microdata - 微数据
		id  ---  必填  ---  ID --- 1
	返回数据：
		result: {
			'status' => 'ok',
			'post_follow_id' => 1,
			'total' => 10,
			'info' => '发布成功'	
		}	

| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| result_num | 10 | 操作数据的ID, 为null时表明不可操作 |
| result_msg | 相关的提示 |  文本 |	


Z-3 我的收藏

	接口地址：
		/user/follow_info
	接口字段：
		user_id  ---  选填  ---  用户ID（希望得到他人的数据时） --- 1
		page --- 必填 --- 分页
		
	返回数据：
		follow_info: [
				{
					target_id: "1",
					user_id: "26056",
					follow_type: "talk",
					follow_time: "1408778874",
					talk_id: "1",
					follow_content: "有想讲的话大家一起说说。?",
					time: "1408775042"
				}
					
		]	
| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| follow_time | 收藏时间  |   |
| follow_type | 收藏类型  |album - 专辑,talk - 桔说,topic - 话题,microdata - 微数据, |
| follow_content | 收藏内容  |   |

Z-5 我的圈子

	接口地址：
		/user/group_info
	接口字段：
		user_id  ---  选填  ---  用户ID（希望得到他人的数据时） --- 1
		page ---  选填  ---- 分页
		
		
	返回数据：
		group_info: [
			{
				id: 1,
            	img: "650b1406baf39292198e3ba5e1e71b06.jpg",
            	title: "2014中国互联网发展广告",
            	info: "万门大学是一个知识分享类大学，是由专业的志愿者原创的、适合互联网学习和使用习惯的免费在线教育平台，2014年被人人网投资和控股。",
            	member_num: "20",
            	topic_num: "40",
            	time: "1398766500",
            	user_id: "26056",
            	edit_time: "1408778475 ",
            	is_in: 1
			}
		]	


Z-6 我的人脉

	接口地址：
		/user/friends_info
	接口字段：
		user_id  ---  选填  ---  用户ID（希望得到他人的数据时） --- 1
		
		
	返回数据：
		friends_info: [
			{
				friends_id: 1,
				friends_logo: "650b1406baf39292198e3ba5e1e71b06.jpg",
				friends_name: "游戏天堂群",
				friends_type: "投资人",
				user_city: " 北京",
				user_prov: " 北京"
			}
		]
| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| friends_id | 人脉ID  |   |
| friends_logo | 人脉头像  |   |
| friends_name | 人脉名称  |   |
| friends_type | 人脉角色  |   |
	
Z-7 关注公司

	接口地址：
		/user/follow_company
	接口字段：
		user_id  ---  选填  ---  用户ID（希望得到他人的数据时） --- 1
		
		
	返回数据：	
		follow_company: [
			{
				com_id: 1,
				com_logo: "650b1406baf39292198e3ba5e1e71b06.jpg",
				com_name: "游戏天堂群",
				scope: {
					id: 18,
					name: "教育培训"
			}
			is_claim: false,
			is_fund: true,
			is_new: true,
			is_hot: false
			}
		]	
| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| com_id | 关注公司ID  |   |
| com_logo | 关注公司LOGO  |   |
| com_name | 关注公司名称  |   |
| scope | 关注公司领域对象 |   |

Z-8 认领公司

	接口地址：
		/user/claim_company
	接口字段：
		user_id  ---  选填  ---  用户ID（希望得到他人的数据时） --- 1
		
		
	返回数据：	
		claim_company: [
			{
				com_id: 1,
				com_logo: "650b1406baf39292198e3ba5e1e71b06.jpg",
				com_name: "游戏天堂群",
				com_claim_title: "CEO",
				com_stage_id: " ",
				scope: {
					id: "14",
					name: "企业服务"
				},
				stage: {
					id: "1",
					name: "初创期"
				},
					comment: {
					num: "6"
				},
					follow: {
					num: "12"
				},
				
				is_claim: true,
				is_fund: true,
				is_new: false,
				is_hot: false
			}
		]		

| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| com_id | 认领公司ID  |   |
| com_logo | 认领公司LOGO  |   |
| com_name | 认领公司名称  |   |
| com_claim_title | 认领人的职位 |   |


Z-9 地区 - 省

	接口地址：
		/address/get_prov
	接口字段：
		
		
	返回数据：	
		all_prov: [
			{
				id: "1",
				city_id: "1",
				city_name: "北京",
				city_p_id: "0"
			},
			{
				id: "2",
				city_id: "2",
				city_name: "上海",
				city_p_id: "0"
			},
			{
				id: "3",
				city_id: "3",
				city_name: "天津",
				city_p_id: "0"
			}
		]		

| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| city_id | 省ID  |   |
| city_name | 省名称  |   |
| city_p_id | 省的父ID |   |

Z-9 地区 - 市

	接口地址：
		/address/get_city
	接口字段：
		id  ---  必填  ---  省ID（希望得到市的省份） --- 1
		
	返回数据：	
		all_city: [
			{
				id: "37",
				city_id: "72",
				city_name: "朝阳区",
				city_p_id: "1"
			}
		]	

| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| city_id | 市ID  |   |
| city_name | 市名称  |   |
| city_p_id | 市的父ID |   |

Z-10　＆＆　Z-1１ 点击自己的技能与意向 （原图有误，第二项为 上IT桔子的目的）
	
	接口地址：
		/user/user_add_custom
	接口字段：
		handle_info  ---  必填  ---  自定义的内容 --- COO|skill|12,找投资|goal|1,找投资|goal
	返回数据：
		user_select_follow: {
			result_num : 10,
			result_msg : "ok"
		}	

| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| result_num | 10 | 数据库中插入的ID, 为null时表明不可操作 |
| result_msg | 相关的提示 |  文本 |	


Z-more

	接口地址：
		/user/my_detail_topic
	接口字段：
		page  ---  选填  ---  第几页 --- 1
		
	返回数据：	
		circle_detail_topic: [
			{
				id: 1,
				time: "1398766500",
				username: "singleseeker",
				img: "650b1406baf39292198e3ba5e1e71b06.jpg",
				user_id: 10,
				topic_type: "新人报道",
				topic_info: "这个主题写的好啊。我真的好喜欢",
				is_liked:0,
				like_num:10,
				comment_num:10,
				topic_circle_id: " 1"
			}
		]


| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| id | 话题ID  |   |
| time | 话题发布时间  |   |
| username | 话题发布者昵秒杀  |   |
| img | 话题发布者logo |   |
| user_id | 话题发布者id  |   |
| topic_type | 话题发布类型  |   |
| time | 话题发布时间  |   |
| topic_info | 话题内容  |   |
| like_num | 赞的人数  |   |
| comment_num | 评论人数  |   |
| is_liked | 是否被赞  |   |
| share_url | 分享地址  |   |




得到所有列表的接口

	接口地址：
		/user/get_user_rel_extend
	接口字段：
		handle_type  ---  必填 ---  类型 --- 技能 skill | 上IT桔子目的 goal
	返回数据：
		get_user_rel_extend: [
			{
				name: "找投资",
				type: "goal",
				id: "1"	
			}
		]

| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| front_user_skill_id 或 front_user_goal_id | ID |  |
| front_user_skill_name 或 front_user_goal_name | 内容 | 

Z-15 用户认证接口

	接口地址：
		/user/user_verrify
	接口字段：
		real_name  ---  必填  ---  真实姓名 --- sss
		address_prov  --- 必填  ---  地址 省 --- 北京
		address_city  --- 必填  ---  地址 市 --- 北京
		email  --- 必填  ---  邮箱 --- 13214124@qq.com
		position  --- 职位  ---  cto
		orag  --- 必填  ---  机构 --- 百度
		img_count  --- 必填  ---  图片总数 --- 13214124
		img_x(数字)  --- 必填  ---  图片字符串
		

	返回数据：
		result: {
			result_num : 10,
			result_msg : "ok"
		}	
字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| result_num | 10 | 数据库中插入的ID, 为null时表明不可操作 |
| result_msg | 相关的提示 |  文本 | 


### 侧栏导航 

不需要API支持

### 圈子

E-1 圈子

	接口地址：
		/circle/all
	接口字段：
		page  ---  选填  ---  第几页 --- 1
		
	返回数据：	
		circle: [
			{
				id: 1,
				img: "650b1406baf39292198e3ba5e1e71b06.jpg",
				title: "2014中国互联网发展广告",
				info: "万门大学是一个知识分享类大学，是由专业的志愿者原创的、适合互联网学习和使用习惯的免费在线教育平台，2014年被人人网投资和控股。",
				member_num: "20",
				topic_num: "40",
				time: "1398766500",
				user_id: "26056",
				edit_time: "1408778475",
				is_in: 1
			}
		]

| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| id | 圈子ID  |   |
| img | 圈子图标  |   |
| title | 圈子标题  |   |
| info | 圈子内容  |   |
| member_num | 圈子成员数  |   |
| topic_num | 圈子标主题数  |   |
| time | 圈子发布时间  |   |

E-more

	接口地址：
		/circle/post_circle_comment
	接口字段：
		info  ---  必填  ---  评论内容 --- 我中一个评论ID
		id ---  必填  ---  话题ID --- 1
	返回数据：
		result: {
			result_num : 10,
			result_msg : "ok"
		}	
字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| result_num | 10 | 数据库中插入的ID, 为null时表明不可操作 |
| result_msg | 相关的提示 |  文本 |


E-2 显示圈子
|

	接口地址：
		/circle/detail_topic
	接口字段：
		page  ---  选填  ---  第几页 --- 1
		id  ---  必填  ---  圈子ID --- 1
		type --- 选填 --- 当type为字符hot时，返回置顶话题 --- hot
		open .
		
	返回数据：	
		circle_detail_topic: [
			{
				id: 1,
				time: "1398766500",
				username: "singleseeker",
				img: "650b1406baf39292198e3ba5e1e71b06.jpg",
				user_id: 10,
				topic_type: "新人报道",
				topic_info: "这个主题写的好啊。我真的好喜欢",
				topic_circle_id: "1",
				share_url: " mobiel/share/topic/2",
				like_num: "1",
				comment_num: "0",
				is_liked: 1
			}
		]


| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| id | 话题ID  |   |
| time | 话题发布时间  |   |
| username | 话题发布者昵秒杀  |   |
| img | 话题发布者logo |   |
| user_id | 话题发布者id  |   |
| topic_type | 话题发布类型  |   |   |
| topic_info | 话题内容  |   |

E-2-1 显示话题评论

	接口地址：
		/circle/detail_comment
	接口字段：
		page  ---  选填  ---  第几页 --- 1
		id  ---  必填  ---  话题ID --- 1
		
	返回数据：	
		microdata_detail_comment: [
			{
				id: 1				
				logo: "650b1406baf39292198e3ba5e1e71b06.jpg",
				username: "wenfeixiang",
				user_id: 10,
				time: "1398766500",
				comment_info: "这个主题写的好啊。我真的好喜欢"
			}
		]


| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| id | 评论ID  |   |
| user_id | 用户ID  |   |
| logo | 评论人头像  |   |
| username | 评论人昵称  |   |
| time | 评论时间  |   |
| comment_info | 评论内容  |   |

E-4 发布话题

	接口地址：
		/circle/publish_circle_topic
	接口字段：
		type  ---  必填  --- 话题类型ID --- "2"
		info  ---  必填  ---  话题内容 --- "万门大学是一个知识分享类大学，是由专业的志愿者原创的、适合互联网学习和使用习惯的免费在线教育平台，2014年被人人网投资和控股。"
		circle_id  ---  必填  ---  圈子ID --- "2"
		
	返回数据：	
		publish_circle_topic: [
			{
				status: "ok",
				'publish_circle_topic_id' => 1,
				info: "发布成功"
			}
		]

| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| status | 发布状态  |   |
| info | 发布状态提示语  |   |

E-4-more 得到话题类型

	接口地址：
		/circle/get_circle_topic_type
	接口字段：
		
	返回数据：	
		topic_type: [
			{
				id: 1,
				name: '我是话题类型'
			}
		]

| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| id | 话题类型ID  |   |
| name | 话题类型名称  |   |


### 微数据

C-1 微数据

	接口地址：
		/microdata/all
	接口字段：
		page  ---  选填  ---  第几页 --- 1
		
	返回数据：	
		microdata: [
			{
				id : 1,
				img: "650b1406baf39292198e3ba5e1e71b06.jpg",
				title: "2014中国互联网发展广告",
				info: "万门大学是一个知识分享类大学，是由专业的志愿者原创的、适合互联网学习和使用习惯的免费在线教育平台，2014年被人人网投资和控股。",
				source: "中国互联网数据研究中心",
				time: "1398766500",
				share_url: 'mobile/share/microdata/1',
				scope_id: "7",
				user_id: " 26056",
				edit_time: "1408778270",
				scope_name: "电子硬件"
			}
		]

| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| id | 微数据ID  |   |
| img | 微数据配图  |   |
| title | 微数据名称  |   |
| info | 微数据内容 |   |
| source | 微数据来源 |   |
| time | 微数据发布时间 |   |
| share_url | 分享的 url |   |

C-more 评论微数据

	接口地址：
		/microdata/post_microdata_comment
	接口字段：
		info  ---  必填  ---  评论内容 --- 我中一个评论ID
		id ---  必填  ---  微数据ID --- 1
	返回数据：
		result: {
			result_num : 10,
			result_msg : "ok"
		}	
字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| result_num | 10 | 数据库中插入的ID, 为null时表明不可操作 |
| result_msg | 相关的提示 |  文本 |	

点赞

	接口地址：
		/company/post_hot
	接口字段：
		handle  ---  必填  ---  点赞还是取消点赞 --- on|off
		id ---  必填  ---  话题子ID --- 1
		type　　---  必填  ---  1桔说｜talk，2话题|topic
	返回数据：
		result: {
			status: "ok",
			post_hot_id: 4,
			total: "4",
			info: "发布成功"
		}	
字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| result_num | 10 | 数据库中插入的ID, 为null时表明不可操作 |
| result_msg | 相关的提示 |  文本 |
| total | 总数 |  文本 |

C-2 微数据

	接口地址：
		/microdata/detail_comment
	接口字段：
		page  ---  选填  ---  第几页 --- 1
		
	返回数据：	
		microdata_detail_comment: [
			{
				id: 1
				logo: "650b1406baf39292198e3ba5e1e71b06.jpg",
				username: "wenfeixiang",
				time: "1398766500",
				comment_info: "这个主题写的好啊。我真的好喜欢",
				user_id: "25362",
			}
		]


| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| id | 评论人ID  |   |
| logo | 评论人头像  |   |
| username | 评论人昵称  |   |
| time | 评论时间  |   |
| comment_info | 评论内容  |   |

C-3 发布微数据

	接口地址：
		/microdata/publish_microdata
	接口字段：
		title  ---  必填  ---  微数据标题 --- "2014中国互联网发展广告"
		source  ---  必填  ---  微数据来源 --- "中国互联网数据研究中心"
		info  ---  必填  ---  微数据内容 --- "万门大学是一个知识分享类大学，是由专业的志愿者原创的、适合互联网学习和使用习惯的免费在线教育平台，2014年被人人网投资和控股。"
		logo  ---  必填  ---  微数据配图 --- "650b1406baf39292198e3ba5e1e71b06.jpg"
		scope  ---  必填  ---  领域ID
		
	返回数据：	
		publish_microdata: [
			{
				status: "ok",
				publish_microdata_id: 10,
				info: "发布成功"
			}
		]

| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| status | 发布状态  |   |
| publish_microdata_id | 发布成功的ID  |   |
| info | 发布状态提示语  |   |

### 动态

	/news/all
	接口字段：
		type  ---  选填  ---  过滤的数据 --- all或不埴为所有， scope 为领域， circle 为圈子
		page  ---  选填  ---  第几页 --- 1
		
	返回数据：
		news:[
			{
				id: 1,
				img: "650b1406baf39292198e3ba5e1e71b06.jpg",
				time: "1398766500",
				title: "在线教育",
				type: "microdata",
				detail: {
				com_id: "1",
				com_name: "安居客梁伟平：是谁逼急了房产商",
				com_des: "安居客梁伟平：是谁逼急了房产商，居客梁伟平：是谁逼急了房产商居客梁伟平：是谁逼急了房产商居客梁伟平：是谁逼急了房产商",
				com_logo: "650b1406baf39292198e3ba5e1e71b06.jpg",
				scope_id: "2",
				is_claim: true,
				is_fund: false,
				is_new: true,
				is_hot: false

				}
			}
		]
| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| type | 数据的形式  |  microdata，company，topic，member |
|  |  |  Todo:其它的形式还得一起商讨 |		

### 桔说

F-1 桔说

	接口地址：
		/talk/all
	接口字段：
		page  ---  选填  ---  第几页 --- 1
		
	返回数据：	
		talk_list: [
			{
				publish_user_id: "1",
				publish_user_nick_name: "颜刘氏",
				talk_info: "词是诗歌的一种，入乐的叫歌，不入乐的叫诗。入乐的歌在感情抒发、形象塑造上和诗没有任何区别，但在结构上、节奏上要受音乐的制约，在韵律上要照顾演唱的",
				talk_comment_num: "20",
				talk_hot: "30",
				img: [],
				publish_time: "1398766500",
				publish_id: "1",
				share_url: 'mobile/share/talk/1',
				is_plus : 1,
				plus_num : 1,
				share_url : 1,
				is_min : 1,
				min_num :1,
				talk_img: "123241234.png ",
				publish_user_nick_name: "穆某某"
				
			}
		]

| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| publish_user_id | 发布者ID  |   |
| publish_user_nick_name | 发布者姓名  |   |
| talk_info | 发布内容  |   |
| talk_comment_num | 评论总数  |   |
| talk_hot | 热度  |   |
| publish_time | 发布时间  |   |
| publish_id | 发布的ID  |   |
| share_url | 分享url  |   |

F-more 

	接口地址：
		/talk/post_talk_comment
	接口字段：
		info  ---  必填  ---  评论内容 --- 我中一个评论ID
		id ---  必填  ---  桔说ID --- 1
	返回数据：
		result: {
			result_num : 10,
			result_msg : "ok"
		}	
字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| result_num | 10 | 数据库中插入的ID, 为null时表明不可操作 |
| result_msg | 相关的提示 |  文本 |		

F-2 发布桔说

	接口地址：
		/talk/publish_talk
	接口字段：
		info  ---  必填  ---  微数据内容 --- "万门大学是一个知识分享类大学，是由专业的志愿者原创的、适合互联网学习和使用习惯的免费在线教育平台，2014年被人人网投资和控股。"
		img_count --- 图片总数 
		img_x(数字) --- 图片单个的字符串
		
	返回数据：	
		publish_talk: [
			{
				status: "ok",
				publish_talk_id: 10,
				info: "发布成功"
			}
		]

| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| status | 发布状态  |   |
| publish_talk_id | 发布成功的ID  |   |
| info | 发布状态提示语  |   |

F-3 桔说

	接口地址
		/talk/handle_hot_num
	接口字段：
		type  ---  必填  ---  做加法，还是关系法 --- (plus | min)
		publish_id   ---  必填  ---  要做运算的主题ID --- 1
		
	返回数据：	
		talk_detail: [
			{
				publish_id: "1",
				result_num: "10",
				result_msg: "ok"
			}
		]
| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| publish_id | 主题的ID  |   |
| result_num | 20 |  为null时表明不可操作 |
| result_msg | 相关的提示 |  文本 |



	接口地址：
		/talk/detail_comment
	接口字段：
		page  ---  选填  ---  第几页 --- 1
		
	返回数据：	
		talk_detail_comment: [
			{
				id: 1
				logo: "650b1406baf39292198e3ba5e1e71b06.jpg",
				username: "wenfeixiang",
				time: "1398766500",
				comment_info: "这个主题写的好啊。我真的好喜欢",
				user_id: "44153",
				nick_id: "43 "
			}
		]


| 字段 | 含义 | 备注 |
| ------------ | ------------- | ---------- |
| id | 评论人ID  |   |
| logo | 评论人头像  |   |
| username | 评论人昵称  |   |
| time | 评论时间  |   |
| comment_info | 评论内容  |   |



